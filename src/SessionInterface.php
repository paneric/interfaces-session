<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Session;

interface SessionInterface
{
    public function sessionStart(?string $jwtSessionId = null): void;

    public function setJwtSessionId(string $jwtSessionId): void;
    public function getJwtSessionId(): ?string;

    public function setData($input, string $key = null): void;
    public function getData(string $key = null): array|string|int|null;
    public function unsetData(string $key = null): void;

    public function setFlash(array $messages, string $key): void;
    public function getFlash(string $key): ?array;

    public function getSessionId(): ?string;
}
