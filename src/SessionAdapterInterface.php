<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Session;

interface SessionAdapterInterface extends SessionInterface
{
    public function openSession(): bool;
    public function closeSession(): bool;
    public function readSession(string $sessionId): string;
    public function writeSession(string $sessionId, string $data): bool;

    public function destroySession(string $sessionId): bool;
    public function gcSession(int $max);
}